# Python Best Practices Cookiecutter (forked repo)

Best practices [cookiecutter](https://github.com/audreyr/cookiecutter) template as described in this [blogpost](https://sourcery.ai/blog/python-best-practices/).

**Improvements/Changes over Sourcery original repo**
- [X] remove github actions
- [X] add gitlab actions
    - [X] post_gen should update the python version of the .gitlab-ci.yml
- [X] refactor code to support Django ORM


## Features
- Testing with [pytest](https://docs.pytest.org/en/latest/)
- Formatting with [black](https://github.com/psf/black)
- Import sorting with [isort](https://github.com/timothycrosley/isort)
- Static typing with [mypy](http://mypy-lang.org/)
- Linting with [flake8](http://flake8.pycqa.org/en/latest/)
- Git hooks that run all the above with [pre-commit](https://pre-commit.com/)
- Deployment ready with [Docker](https://docker.com/)


## Quickstart
```sh
# create a virtualenv and install cookiecutter
pipenv install
pipenv install cookiecutter

# Use cookiecutter to create project from this template
cookiecutter git@gitlab.com:vsf-pocs/python_cookiecutter.git -o <dir_path>

# Enter project directory
cd <dir_path>

# Initialise git repo
git init

# Install dependencies
pipenv install --dev

# Setup pre-commit and pre-push hooks
pre-commit install -t pre-commit
pre-commit install -t pre-push
```
