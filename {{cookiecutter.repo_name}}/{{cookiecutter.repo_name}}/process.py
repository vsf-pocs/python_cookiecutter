# setting up django with the following import
from django.db import connections
from django.db.utils import OperationalError

from {{cookiecutter.repo_name}} import manage
# import models here if needed


def check_db_connection():
    db_conn = connections["default"]
    try:
        db_conn.cursor()
    except OperationalError:
        return False
    else:
        return True
