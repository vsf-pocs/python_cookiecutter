from {{cookiecutter.repo_name}}.process import check_db_connection
from argparse import ArgumentParser
import logging


def parse_args(args):
    parser = ArgumentParser()
    parser.add_argument("--smoke_test", default=False, action='store_true',
                        help="checks if container and DB are healthy")

    return parser.parse_args(args)


def main(args):
    options = parse_args(args)

    if options.smoke_test:
        if check_db_connection():
            logging.info("Database connection check is successful!")
        else:
            logging.info("Database connection error! Check Django settings.py")


if __name__ == "__main__":
    import sys

    logging.basicConfig(
        level=logging.INFO,
        format='%(asctime)s.%(msecs)03d %(levelname)s %(module)s - %(funcName)s: %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S'
    )

    main(sys.argv[1:])
