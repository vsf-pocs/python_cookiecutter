# DJANGO imports and setup needs to be in perfect order (isort must NOT re-order == # noqa: F401)
import sys  # noqa: F401

sys.dont_write_bytecode = True  # noqa: F401
import os  # noqa: F401

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "{{cookiecutter.repo_name}}.settings")  # noqa: F401
import django  # noqa: F401

django.setup()  # noqa: F401


if __name__ == "__main__":
    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
