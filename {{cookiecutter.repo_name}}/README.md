# {{cookiecutter.project_name}}

## Setup
```sh
# Install dependencies
pipenv install --dev

# Setup pre-commit and pre-push hooks
pre-commit install -t pre-commit
pre-commit install -t pre-push
pipenv shell
```

## Credits
This package was created with Cookiecutter
- original reference: [sourcery-ai/python-best-practices-cookiecutter](https://github.com/sourcery-ai/python-best-practices-cookiecutter)
- modified repo: [vsf-pocs/python_cookiecutter.git](https://gitlab.com/vsf-pocs/python_cookiecutter)
